from configparser import ConfigParser

# default values
default_config = """
[default]
reference_images = reference_images/FOV_{fov:03d}_refbeads.tif
alignment_stack = alignment_stack/ALIGN_{fov:03d}_refbeads.tif
"""


def read_config(config=None):
    config = config or default_config
    conf = ConfigParser()
    conf.read_string(config)
    return conf["default"]
