import io
import sys
from argparse import ArgumentParser, FileType


def parse_args(args):
    parser = ArgumentParser()
    parser.add_argument(
        "--host",
        required=False,
        type=str,
        default="localhost",
        help="Hostname or IP where to run the server",
    )
    parser.add_argument(
        "--port",
        required=False,
        type=int,
        default=10000,
        help="Port to bind the socket to",
    )
    parser.add_argument(
        "--log", required=False, type=str, help="File name if logging to file"
    )
    parser.add_argument(
        "--conf", required=False, type=FileType("r"), help="Configuration file"
    )
    parser.add_argument(
        "--debug",
        required=False,
        action="store_true",
        help="Debug mode (more verbose logging",
    )
    return parser.parse_args(args)


def main():  # pragma: nocover
    """Main command line entrypoint.
    """
    args = parse_args(sys.argv[1:])

    from .server import Server
    from .log import setup_logging

    setup_logging(debug=args.debug, filename=args.log)

    if not args.conf:
        args.conf = io.StringIO()

    with Server(args.host, args.port, conf=args.conf.read()) as server:
        server.start()
