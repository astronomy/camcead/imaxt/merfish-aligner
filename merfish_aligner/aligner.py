import logging

import bottleneck as bn
import numpy as np
import scipy.ndimage as nd
from astropy.nddata import NDData
from astropy.table import Table
from photutils import find_peaks
from photutils.morphology import data_properties
from photutils.psf import extract_stars

log = logging.getLogger("merfish-aligner")


def background_median(img, size=32):  # pragma:  nocover
    med1 = nd.filters.median_filter(img, size=(1, size))
    med2 = nd.filters.median_filter(img, size=(size, 1))
    return (med1 + med2) / 2


def background_uniform(img, size=32):  # pragma:  nocover
    med1 = nd.filters.uniform_filter1d(img, size=size, axis=0)
    med2 = nd.filters.uniform_filter1d(img, size=size, axis=1)
    return (med1 + med2) / 2


def background_bn(img, size=32):
    med1 = bn.move_median(img, window=size, axis=0)
    med2 = bn.move_median(img, window=size, axis=1)
    return (med1 + med2) / 2


class Aligner:
    def __init__(self, tbl, z=None):
        self.tbl = tbl
        self.z = z

    def compare(self, aligner):
        amplitude = np.median(
            np.absolute(self.tbl["amplitude"] - aligner.tbl["amplitude"])
        )
        x_stddev = np.median(
            np.absolute(self.tbl["x_stddev"] - aligner.tbl["x_stddev"])
        )
        y_stddev = np.median(
            np.absolute(self.tbl["y_stddev"] - aligner.tbl["y_stddev"])
        )
        x_offset = np.median(self.tbl["x_mean"] - aligner.tbl["x_mean"])
        y_offset = np.median(self.tbl["y_mean"] - aligner.tbl["y_mean"])
        theta = np.median(np.absolute(self.tbl["theta"] - aligner.tbl["theta"]))
        res = {
            "amplitude": amplitude,
            "x_stddev": x_stddev,
            "y_stddev": y_stddev,
            "x_offset": x_offset,
            "y_offset": y_offset,
            "theta": theta,
            "z": aligner.z,
        }
        return res

    @staticmethod
    def detect_beads(arr, **kwargs):
        peaks_tbl = find_peaks(arr, **kwargs)
        return peaks_tbl

    @classmethod
    def from_image(cls, img: np.ndarray, tbl=None, maxstars=20, z=None):
        log.debug("Fitting background")
        img = img.astype('float')
        bkg = background_bn(img, size=64)
        np.nan_to_num(bkg, copy=False)

        # Detect peaks
        arr = img - bkg
        garr = nd.gaussian_filter(arr, 1.5)
        mad = 1.48 * np.median(np.absolute(garr))
        log.debug("Image standard deviation %.2f", mad)
        if tbl is None:
            for threshold in [100, 75, 50, 25]:
                log.debug("Looking for peaks above %.2f", threshold * mad)
                peaks_tbl = cls.detect_beads(
                    garr, threshold=threshold * mad, box_size=30, border_width=100
                )
                if len(peaks_tbl) > 10:
                    break

            if len(peaks_tbl) < 10:
                raise Exception("Not enough beads found in image")

            log.debug("Beads found %d", len(peaks_tbl))

            # Make a astropy Table to extract cutouts
            stars_tbl = Table()
            x, y, peak = (
                peaks_tbl["x_peak"],
                peaks_tbl["y_peak"],
                peaks_tbl["peak_value"],
            )
            indx = np.argsort(peak)[-maxstars:][::-1]
            stars_tbl["x"] = x[indx]
            stars_tbl["y"] = y[indx]
            stars_tbl["peak"] = peak[indx]
        else:
            stars_tbl = tbl.copy()

        # Extract 30 pix cutouts
        nddata = NDData(data=garr)
        stars = extract_stars(nddata, stars_tbl, size=30)
        log.debug("Fitting extracted beads")

        fit = [data_properties(s) for s in stars]

        stars_tbl["amplitude"] = [f.max_value for f in fit]
        stars_tbl["x_mean"] = [f.xcentroid.value for f in fit]
        stars_tbl["y_mean"] = [f.ycentroid.value for f in fit]
        stars_tbl["x_stddev"] = [f.semimajor_axis_sigma.value for f in fit]
        stars_tbl["y_stddev"] = [f.semimajor_axis_sigma.value for f in fit]
        stars_tbl["theta"] = [f.ellipticity.value for f in fit]

        return cls(stars_tbl, z=z)
