import logging
import logging.handlers
import sys


def setup_logging(debug: bool = False, filename: str = None):  # pragma: nocover
    if debug:
        level = logging.DEBUG
        fmt = (
            "%(asctime)s %(name)s: %(levelname)s %(filename)s "
            "%(funcName)s (%(lineno)d): %(message)s [%(threadName)s]"
        )
    else:
        level = logging.INFO
        fmt = "%(asctime)s %(name)s: %(levelname)s %(message)s [%(threadName)s]"

    if filename:
        fh = logging.handlers.TimedRotatingFileHandler(
            filename, when="d", backupCount=7
        )
        logging.basicConfig(level=level, format=fmt, handlers=[fh])
    else:
        logging.basicConfig(level=level, format=fmt, stream=sys.stderr)

    return
