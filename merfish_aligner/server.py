import json
import logging
import math
import socket
from concurrent.futures import ProcessPoolExecutor, as_completed
from contextlib import suppress
from pathlib import Path
from typing import Any, Dict, Union

from imaxt_image.image import TiffImage

from .aligner import Aligner
from .conf import read_config

log = logging.getLogger("merfish-aligner")


class Message:
    """Message envelope.

    Parameters
    ----------
    data
        Received data
    """

    def __init__(self, data: bytearray):
        self.msg = data.decode()
        self.type, *content = self.msg.split(maxsplit=1)
        self.type = self.type.upper()
        if content:
            self.content = content[0].strip()
        else:
            self.content = None

    def __repr__(self):
        return f"{self.type}: {self.content}".strip()


class Server:
    """MERFISH Aligner Server.

    The server listens to the given IP and port for incomming messages.

    Parameters
    ----------
    host
        Hostname or IP
    port
        Port to listen on
    conf
        String containing configuration
    """

    def __init__(self, host: str, port: int, conf: str = None):
        self.host = host
        self.port = port
        self._shutdown = False

        try:
            self._conf = read_config(conf)
        except Exception:
            raise Exception("Error reading configuration file")

        if ("reference_images" not in self._conf) or (
            "alignment_stack" not in self._conf
        ):
            raise Exception("Error reading configuration file")

        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

    def __enter__(self):
        return self

    def __exit__(self, exception_type, exception_value, traceback):
        self.socket.close()

    def start(self):
        """Start server.
        """
        log.info("Starting listening on %s:%d", self.host, self.port)
        self.socket.bind((self.host, self.port))
        self.socket.listen(1)
        self.executor = ProcessPoolExecutor()
        log.info("Using %d workers", self.executor._max_workers)
        with suppress(Exception):
            self.read_message_loop()

    def read_message_loop(self):
        """Read message loop.
        """
        while not self._shutdown:
            connection, client_address = self.socket.accept()
            log.info("Connection from %s", client_address)
            try:
                while True:
                    data = connection.recv(256)
                    if data:
                        msg = Message(data)
                        log.info("Received %s", msg)
                        if msg.type == "STOP":
                            self._shutdown = True
                            log.info("Stopping server")
                            break
                        else:
                            res = self.process_message(msg)
                            if res:
                                res = json.dumps(res)
                                connection.sendall(res.encode("utf-8"))
                    else:
                        log.info("Disconnected %s", client_address)
                        break
            finally:
                connection.close()

    def process_message(self, msg):
        """Process message
        """
        if msg.type == "CWD":  # store experiment directory
            cwd = Path(msg.content)
            if not cwd.exists():
                log.error("Experiment directory does not exist.")
            else:
                self._cwd = cwd
        if msg.type == "REF":  # read reference image and analyse
            try:
                self.process_message_ref(msg.content)
            except Exception as e:
                log.error(f"{e}")
        elif msg.type == "STACK":  # read stack of images and analyse
            try:
                best = self.process_message_stack(msg.content)
            except Exception as e:
                log.error(f"{e}")
                best = {"status": "error", "error": f"{e}"}
            return best

    def process_message_ref(self, msg: str) -> None:
        """Process REF message.
        """
        try:
            fov = int(msg)
            filename = self._cwd / \
                self._conf["reference_images"].format(fov=fov)
        except AttributeError:
            err = "Experiment directory not set. Ignoring REF message."
            raise Exception(err)
        except ValueError:
            filename = Path(msg)

        self.set_zref(filename)

    def process_message_stack(self, msg: str) -> Union[None, Dict]:
        """Process STACK message.
        """
        try:
            self._aligner_ref
            fov = int(msg)
        except AttributeError:
            err = "Reference image not set. Ignoring STACK message."
            raise Exception(err)
        except ValueError:
            filename = Path(msg)
        else:
            filename = self._cwd / self._conf["alignment_stack"].format(fov=fov)

        best = self.analyse_stack(filename)
        log.info("Best focus %s", best)
        return best

    def set_zref(self, filename: Path) -> None:
        """Set the reference focus.

        Parameters
        ----------
        filename
            File name of TIFF containing reference focus image.
        """
        if not filename.exists():
            log.error("File does not exist %s", filename)
            return

        log.debug("Reference image name %s", filename)
        try:
            img = TiffImage(filename).asarray()
            log.debug("Reference image size %s", img.shape)
        except ValueError:
            log.error("Not a TIFF file: %s", filename)
            return
        except:  # noqa: E722  # pragma: nocover
            log.exception("Unknown error reading %s", filename)
            return
        else:
            future = self.executor.submit(Aligner.from_image, img)

        if future.exception():  # pragma: nocover
            log.error("Reference image analysis failed: %s", future.exception())
        else:
            log.info("Reference image analysis done")
            self._aligner_ref = future.result()
        future.cancel()

    def analyse_stack(self, filename: Path) -> Dict[str, Any]:
        """Analyse stack of focus images and determine which one is
        more similar to the reference.

        Parameters
        ----------
        filename
            Name of the TIFF file containing the stack of focus.

        Returns
        -------
        Dictionary containing the best focus parameters.
        """
        if not filename.exists():
            log.error("File does not exist %s", filename)
            return {"status": "error"}

        log.debug("Stack image name %s", filename)

        try:
            img = TiffImage(filename).asarray()
            log.debug("Stack image size %s", img.shape)
        except ValueError:
            log.error("Not a TIFF file: %s", filename)
            return {"status": "error"}

        futures = []
        for z, im in enumerate(img):
            future = self.executor.submit(
                Aligner.from_image, im, tbl=self._aligner_ref.tbl, z=z
            )
            futures.append(future)

        best = {
            "z": 0,
            "sigma": 99,
            "x_offset": 0,
            "y_offset": 0,
            "status": "done",
            "error": "",
        }
        for future in as_completed(futures):
            if future.exception():  # pragma: nocover
                log.error("Stack image %d analysis failed %s",
                          z, future.exception())
                best["error"] = best["error"] + future.exception()
            else:
                aligner_stack = future.result()
                z = aligner_stack.z
                diff = self._aligner_ref.compare(aligner_stack)
                amplitude = diff["amplitude"]
                theta = diff["theta"]
                sigma = math.sqrt(diff["x_stddev"] ** 2 + diff["y_stddev"] ** 2)
                if sigma < best["sigma"]:
                    best.update(
                        {
                            "z": z,
                            "sigma": sigma,
                            "x_offset": -diff["x_offset"],
                            "y_offset": -diff["y_offset"],
                        }
                    )
                log.info(
                    "Stack image %d analysis done: %.2f, %.4f, %4f",
                    z,
                    amplitude,
                    sigma,
                    theta,
                )
            future.cancel()
        return best
