from setuptools import find_packages, setup

with open("README.md") as readme_file:
    readme = readme_file.read()

requirements = [
    "astropy",
    "photutils==0.6",
    "scipy",
    "numpy",
    "imaxt-image>=0.2",
    "bottleneck"
]

setup_requirements = ["pytest-runner", "flake8"]

test_requirements = ["coverage", "pytest", "pytest-cov", "pytest-mock"]

setup(
    author="IMAXT Team",
    author_email="eglez@ast.cam.ac.uk",
    classifiers=[
        "Development Status :: 4 - Beta",
        "Intended Audience :: Developers",
        "License :: OSI Approved :: GNU General Public License v3 (GPLv3)",
        "Programming Language :: Python :: 3.5",
        "Programming Language :: Python :: 3.6",
        "Programming Language :: Python :: 3.7",
    ],
    description="IMAXT MERFISH Aligner",
    entry_points={"console_scripts": ["aligner=merfish_aligner.cli:main"]},
    install_requires=requirements,
    license="GNU General Public License v3",
    long_description=readme,
    keywords="imaxt, merfish",
    name="merfish-aligner",
    packages=find_packages(include=["merfish_aligner*"]),
    setup_requires=setup_requirements,
    test_suite="tests",
    tests_require=test_requirements,
    url="https://gitlab.ast.cam.ac.uk/imaxt/merfish-aligner",
    version="0.7.0",
    zip_safe=False,
    python_requires=">=3.5",
)
