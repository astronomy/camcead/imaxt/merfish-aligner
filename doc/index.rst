.. IMAXT Pipeline documentation master file, created by
   sphinx-quickstart on Sat Oct 27 05:47:17 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to IMAXT Pipeline's documentation!
==========================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   source/modules

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
