# MerFISH Aligner

The merFISH Aligner is a script running on the merFISH image acquisition machine 
to allow for on-the-fly re-focusing and re-alignment of each FOV.

This is how it works:

1. Upon start of the merFISH protocol, the microscope starts a Python program 
listening to a TCP socket.

2. The microscope sends a ``CWD`` command containing the location of the directory 
where images are saved (see below).

3. Before the first cycle of merFISH imaging, the microscope acquires one bead 
image for each FOV and saves them in a subdirectory of the experiment folder. 
These would become the “reference” image for that FOV.

4. For each FOV the telescope sends a ``REF`` command with the FOV number. 
The Aligner loads the reference image for that particular FOV,
by default located in ```reference_images/FOV_{fov:03d}_refbeads.tif```.
The Aligner detects suitable beads and parameterizes them.

5. For each cycle of each FOV the microscope acquires a stack of bead 
images and saves them to a file in a subdirectory. 

6. The microscope sends a ``STACK`` command with the FOV number.

7. The Aligner loads the stack of beads images, by default located in
```alignment_stack/ALIGN_{fov:03d}_refbeads.tif```, determines the 
correct focus Z and (X, Y) offsets.

8. The Aligner sends a response to the microscope using the same TCP socket 
containing the correct Z focus (the index in the stack, starting at 0) and the
(X, Y) displacement in pixels.

9. The microscope selects the correct Z focus and converts (X, Y) to stage
coordinates. After this the cycle is acquired and saved normally.

10. Within a single FOV, for each cycle repeat from step 5.

11. For each FOV repeat from step 4.

12. After the merFISH protocol ends, the microscope stops the Aligner server 
sending a ``STOP`` command.

## Installation

This software requires at least Python 3.5.

Using pip (assuming the IMAXT repo is configured (see https://gitlab.ast.cam.ac.uk/snippets/1):

    pip install merfish-aligner 

If the package is already installed, upgrade to the latest version using

    pip install -U merfish-aligner
    
## Usage

### Server

    $ aligner -h
    usage: aligner [-h] [--host HOST] [--port PORT]

    optional arguments:
    -h, --help   show this help message and exit
    --host HOST  Hostname or IP where to run the server
    --port PORT  Port to bind the socket to
    --log LOG    File name if logging to file
    --conf CONF  Configuration file
    --debug      Debug mode (more verbose logging

The server opens a connection to a socket and waits for commands. 
Start the aligner server:

    $ aligner --host localhost --port 10000

The command above prints logging message to standard output. In order
to log messages to a file use:

    $ aligner --log /path/to/aligner.log --debug

where the extra ``--debug`` argument can be used to increase the verbosity
and detail of the messages.

without the command line arguments the server will be started in 
port 10000 of localhost.

Commands accepted are:

- Send location of directory where images are saved (this only needs to be
  done once per experiment).

      CWD /path/to/dir

- Trigger analysis on reference image for FOV 1. This is located in 
  ```/path/to/dir/reference_images/FOV_001_refbeads.tif```
      
      REF 1

  For the command to be successful, a ```CWD``` command must have been sent before 
  sending the ```REF```. As an alternative the ```REF``` command can include 
  the full filename as in:

      REF /path/to/ref_z.tif

  In this case ```CWD``` is not required.

- Trigger analysis on the stack of beads images for FOV 1. This is located in
  ```/path/to/dir/alignment_stack/ALIGN_001_refbeads.tif```

      STACK 1

  For the command to be successful both ```CWD``` and ```REF``` commands must have
  been sent. As an alternative the ```STACK``` command can include the full filename as in:

      STACK /path/to/stack_z.tif

  In this case ```CWD``` is not required.

- Shutdown the server:

      STOP

After a ``STACK`` command, the server answers with a string containing
the best focus and the (x,y) offsets::

    {"z": 4, "sigma": 0.098, "x_offset": -2.81, 
     "y_offset": 3.45, "status": "done"}

The offsets are those to apply to the image in the stack to put it back into the 
reference coordinates.

In case of error the returned string will contain::

    {"status": "error"}

### Logging

By default logging goes to standard output. Use the ```--log``` argument to specify
an output file. 

### Configuration

The deafult location of reference and stack images (see above) wen using numeric
```REF``` and ```STACK``` commands can be modified
with the optional ```--conf``` argument. Given a configuration file like:

```
[default]
reference_images = reference_images/FOV_{fov:03d}_refbeads.tif
alignment_stack = alignment_stack/ALIGN_{fov:03d}_refbeads.tif
```

The configuration is then used when starting the aligner as:

    $ aligner --conf /path/to/filename.conf

### Client

The client should ensure that messages are sent with length of 256 characters.
A simple client to interact with the server in Python:

```python
import json
import socket

class Client:
    def __init__(self, host, port):
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket.connect((host, port))

    def send(self, msg):
        s = f"{msg:256s}"
        res = self.socket.send(s.encode("utf-8"))
        return res

    def close(self):
        self.socket.close()

    def recv(self):
        res = self.socket.recv(256).decode()
        return json.loads(res)

    def __enter__(self):
        return self

    def __exit__(self, exception_type, exception_value, traceback):
        self.close()
```

For example:

```python
with Client("localhost", 10000) as client:
    client.send("REF /path/to/ref_z.tif")
    client.send("STACK /path/to/stack_z.tif")
    res = client.recv()
    
zbest = res["z"]  # best focus image (index starts in 0)
xoff, yoff = res["x_offset"], res["y_offset"]  # offsets
```

