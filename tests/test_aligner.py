import os
from pathlib import Path
from unittest import mock

import pytest
from pytest import approx
from scipy.ndimage import shift

from imaxt_image.image import TiffImage
from merfish_aligner.aligner import Aligner
from merfish_aligner.log import setup_logging

setup_logging(debug=True)


def test_aligner_ref():
    dname = os.path.dirname(__file__)
    img = TiffImage(Path(dname) / Path("data/fov_001_refbeads.tif")).asarray()
    aligner = Aligner.from_image(img)

    assert len(aligner.tbl) > 10
    for col in [
        "x",
        "y",
        "peak",
        "amplitude",
        "x_mean",
        "y_mean",
        "x_stddev",
        "y_stddev",
        "theta",
    ]:
        assert col in aligner.tbl.colnames


@pytest.mark.filterwarnings(
    "ignore:.*unsuccessful.*:astropy.utils.exceptions.AstropyUserWarning"
)
def test_aligner_offset():
    dname = os.path.dirname(__file__)
    img = TiffImage(Path(dname) / Path("data/fov_001_refbeads.tif")).asarray()
    aligner = Aligner.from_image(img)

    offimg = shift(img, (3.0, 4.0))
    offaligner = Aligner.from_image(offimg, tbl=aligner.tbl)

    res = aligner.compare(offaligner)
    assert res["x_offset"] == approx(-4, 0.5)
    assert res["y_offset"] == approx(-3, 0.5)

    newimg = shift(offimg, (res["y_offset"], res["x_offset"]))
    newaligner = Aligner.from_image(newimg, tbl=aligner.tbl)
    res = aligner.compare(newaligner)

    assert res["x_offset"] == approx(0.0, abs=0.5)
    assert res["y_offset"] == approx(0.0, abs=0.5)


@mock.patch.object(Aligner, "detect_beads", return_value=[])
def test_aligner_nobeads(mocker_args):
    with pytest.raises(Exception) as excinfo:
        dname = os.path.dirname(__file__)
        img = TiffImage(
            Path(dname) / Path("data/fov_001_refbeads.tif")).asarray()
        Aligner.from_image(img)

    assert "Not enough beads found" in f"{excinfo}"
