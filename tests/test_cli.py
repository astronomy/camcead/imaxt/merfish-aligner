

from merfish_aligner.cli import parse_args


def test_cli():
    args = parse_args(["--debug", "--log", "logfile"])
    assert args.debug == True  # noqa: E712
    assert args.log == "logfile"
    assert args.host == "localhost"
    assert args.port == 10000
