import json
import os
import socket
import threading
import time
from contextlib import closing

import pytest
from pytest import approx

from merfish_aligner.log import setup_logging
from merfish_aligner.server import Server

setup_logging(debug=True)

test_config = """
[default]
reference_images = data/fov_{fov:03d}_refbeads.tif
alignment_stack = data/align_{fov:03d}_refbeads.tif
"""


class A:
    pass


def find_free_port():
    with closing(socket.socket(socket.AF_INET, socket.SOCK_STREAM)) as s:
        s.bind(("", 0))
        s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        return s.getsockname()[1]


@pytest.fixture
def default_server():
    host = "127.0.0.1"
    port = find_free_port()
    A.port = port

    with Server(host, port, conf=test_config) as server:
        server_thread = threading.Thread(target=server.start)
        server_thread.daemon = True
        server_thread.start()
        time.sleep(1)
        yield server_thread


@pytest.fixture
def default_client():
    host = "127.0.0.1"
    port = A.port
    with Client(host, port) as client:
        yield client


class Client:
    def __init__(self, host, port):
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket.connect((host, port))

    def send(self, msg):
        s = f"{msg:256s}"
        res = self.socket.send(s.encode("utf-8"))
        return res

    def close(self):
        self.socket.close()

    def recv(self):
        res = self.socket.recv(256).decode()
        return json.loads(res)

    def __enter__(self):
        return self

    def __exit__(self, exception_type, exception_value, traceback):
        self.close()


def test_server_connection(default_server, default_client):
    default_client.send("STOP")


@pytest.mark.filterwarnings(
    "ignore:.*unsuccessful.*:astropy.utils.exceptions.AstropyUserWarning"
)
def test_server_ref(default_server, default_client):
    dname = os.path.dirname(__file__)
    default_client.send(f"CWD {dname}")
    default_client.send(f"REF 1")
    default_client.send(f"STACK 1")

    best = default_client.recv()
    assert best["status"] == "done"
    assert best["z"] == 4


def test_server_noref(default_server, default_client):
    dname = os.path.dirname(__file__)
    default_client.send(f"CWD {dname}")
    default_client.send(f"STACK 1")
    best = default_client.recv()
    assert best["status"] == "error"


def test_server_notiff(default_server, default_client):
    dname = os.path.dirname(__file__)
    default_client.send(f"CWD {dname}")
    default_client.send(f"REF 12")

    default_client.send(f"REF 1")
    default_client.send(f"STACK 12")
    best = default_client.recv()
    assert best["status"] == "error"


def test_server_cwd_not_exists(default_server, default_client):
    dname = os.path.dirname(__file__) + "nodata"
    default_client.send(f"CWD {dname}")


def test_server_nocwd(default_server, default_client):
    default_client.send(f"REF 1")


def test_server_invalid_ref(default_server, default_client):
    dname = os.path.dirname(__file__)
    default_client.send(f"CWD {dname}")
    default_client.send(f"REF aaa")


def test_server_invalid_stack(default_server, default_client):
    dname = os.path.dirname(__file__)
    default_client.send(f"CWD {dname}")
    default_client.send(f"REF 1")
    default_client.send(f"STACK aaa")
    best = default_client.recv()
    assert best["status"] == "error"


def test_server_wrong_format(default_server, default_client):
    dname = os.path.dirname(__file__)
    default_client.send(f"CWD {dname}")
    default_client.send(f"REF 2")

    default_client.send(f"CWD {dname}")
    default_client.send(f"REF 3")
    default_client.send(f"STACK 3")
    best = default_client.recv()
    assert best["status"] == "error"


def test_server_wrongconf():
    with pytest.raises(Exception):
        Server("localhost", 7777, conf="aaa")

    with pytest.raises(Exception):
        Server("localhost", 7777, conf="[default]\na = 1")


@pytest.mark.filterwarnings(
    "ignore:.*unsuccessful.*:astropy.utils.exceptions.AstropyUserWarning"
)
def test_server_paths(default_server, default_client):
    dname = os.path.dirname(__file__)
    default_client.send(f"REF {dname}/data/fov_001_refbeads.tif")
    default_client.send(f"STACK {dname}/data/align_001_refbeads.tif")
    best = default_client.recv()
    assert best["status"] == "done"
    assert best["z"] == 4
